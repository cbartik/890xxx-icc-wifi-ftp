
// recieve command test
// to test send commqqand

#include <CmdMessenger.h>  // CmdMessenger
#include <Ticker.h>
#include "FS.h"
#include <ESP8266WiFi.h>

CmdMessenger cmdMessenger = CmdMessenger(Serial);     // Attach a new CmdMessenger object to the default Serial port
//67.158.61.56,tms-user,tmsftp,tmsdir/test
//ICC,Iccwifi1234^Z

Ticker     ticker;
File       f;

WiFiClient ftp_cmd_client;
WiFiClient ftp_data_client;

#define   RED_LED_PIN       0
#define   BLUE_LED_PIN      2

bool      ledState      = 0;   // Current state of Led
bool      blink_flag    = 0;
int       blink_cntr;
boolean   ticker_reached;
bool      debug_flag    = 0;
char*     file_name;              //file name
char*     ftp_file_name;

bool      wifi_status_check;
String    wifi_ssid;
String    wifi_password;

bool      ftp_open_flag;
String    ftp_ip;
String    ftp_un;
String    ftp_pw;
String    ftp_dir;

char outBuf[128];
char outCount;

// ======================== Set up ===============================
void setup()
{
    Serial.begin(115200);
    cmdMessenger.printLfCr();           // Adds newline to every command
    attachCommandCallbacks();           // Attach my application's user-defined callback methods

    ticker.attach(0.5, ticker_handler); //call ticker_handler() in 0.5 second
    ticker_reached = false;

    ftp_open_flag=false;

    if ( !SPIFFS.begin() ) Serial.println("Failed to mount file system");
   
    pinMode(RED_LED_PIN, OUTPUT);        // set pin for blink LED
    digitalWrite(RED_LED_PIN, 0);
    pinMode(BLUE_LED_PIN, OUTPUT);
    Serial.println(" ");
    Serial.println("ICC Wifi FTP V0.27.02");

}

// ======================== Main ===============================
void loop()
{
    cmdMessenger.feedinSerialData();      // Process incoming serial data, and perform callbacks

   if (blink_flag == 1) blink_red_led();
   if (wifi_status_check) wifi_blue_led_check(); 
}

// =============interupt ticker========================
void ticker_handler()
{
  ticker_reached = true;
  wifi_status_check=true;
}
// =============main functins========================
void  blink_red_led()
{
  if (blink_flag == 1 && ticker_reached)
  { digitalWrite(RED_LED_PIN, ledState);
    ledState = !ledState;
    //Serial.print(".");
    ticker_reached = false;
  }
}
// ==========
void  wifi_blue_led_check()
{   
    wifi_status_check=0;  
    if  ( WiFi.status() == WL_CONNECTED )
    {    digitalWrite(BLUE_LED_PIN, 0);   //blue led on
    }else
    {    digitalWrite(BLUE_LED_PIN, 1);   //blue led off
    }  
}
// ======================== Command routines ===============================
// ========= command numbers ======
enum
{
  BADCOMMAND    =0,     //Bad command   0
  HELPCOMMAND,          //Help Command  1
  TOGGLELED,            //test cmd Toggle red led 2
  BLINKLED,             //test cmd Blink red led  3
  DEBUGCMD,             //test cmd debug toggle   4
 
  SPIFFSdir     =10,    //directory of spifs command    10
  SPIFFSinfo,           //print out spiffs information  11
  SPIFFSr,              //read spiffs file command      12
  SPIFFSw,              //write spiffs file command     13
  SPIFFSdel,            // delete file command          14
  SPIFFSfmt     =19,    // format file system           19
  
  WIFI_UN_PS    =20,    // wifi user name & password  20
  WIFI_CONNECT,         // wifi connect               21
  WIFI_DISCONNECT,      // wifi disconnect            22
  WIFI_STATUS,          // wifi status                23
  WIFI_SCAN,            //wifi scan for networks      24

  FTP_UN_PW     =30,    // ftp set user name and PW   30
  FTP_CONNECT,          // ftp connect to ftp site    31
  FTP_CLOSE,            // ftp close                  32
  FTP_UPLOAD,           // ftp upload                 33
  FTP_DOWNLOAD,         // ftp download               34
  FTP_DIRECTORY,        // ftp directory              35
  FTP_DELETE,           // ftp delete file            36

  DEEP_SLEEP   =40,     //sleep command               40
};

// ============== Callbacks conected to commands ===========
void attachCommandCallbacks()
{
  cmdMessenger.attach(cmd0_bad);                    //excecutes when unknown number is entered
  cmdMessenger.attach(BADCOMMAND,   cmd0_bad);
  cmdMessenger.attach(HELPCOMMAND,  cmd1_help);
  cmdMessenger.attach(TOGGLELED,    cmd2_toggleled);
  cmdMessenger.attach(BLINKLED,     cmd3_blinkled);
  cmdMessenger.attach(DEBUGCMD,     cmd4_debug);

  
  cmdMessenger.attach(SPIFFSdir,    cmd10_dir_spiffs_file);
  cmdMessenger.attach(SPIFFSinfo,    cmd11_spiffs_info);
  cmdMessenger.attach(SPIFFSr,      cmd12_read_spiffs_file);
  cmdMessenger.attach(SPIFFSw,      cmd13_write_spiffs_file);
  cmdMessenger.attach(SPIFFSdel,    cmd14_delete_file);
  cmdMessenger.attach(SPIFFSfmt,    cmd19_format_spiffs);

  cmdMessenger.attach(WIFI_UN_PS ,    cmd20_wifi_unpw);
  cmdMessenger.attach(WIFI_CONNECT,   cmd21_wifi_connect);
  cmdMessenger.attach(WIFI_DISCONNECT,cmd22_wifi_disconnect);
  cmdMessenger.attach(WIFI_STATUS,    cmd23_wifi_status);
  cmdMessenger.attach(WIFI_SCAN,      cmd24_wifi_scan);

  cmdMessenger.attach(FTP_UN_PW,      cmd30_ftp_ip_un_pw);
  cmdMessenger.attach(FTP_CONNECT,    cmd31_ftp_connect);
  cmdMessenger.attach(FTP_CLOSE,      cmd32_ftp_close);
  cmdMessenger.attach(FTP_UPLOAD,     cmd33_ftp_upload);
  cmdMessenger.attach(FTP_DOWNLOAD,   cmd34_ftp_download);
  cmdMessenger.attach(FTP_DIRECTORY,  cmd35_ftp_dir);
  cmdMessenger.attach(FTP_DELETE,     cmd36_ftp_delete);

  cmdMessenger.attach(DEEP_SLEEP,   cmd40_deep_sleep);

   
}
// ==================    command callback functions ====================
void cmd0_bad()
{ Serial.println("ERR");
}
//=========
void cmd1_help()
{   
    
    Serial.println("===== Test =====");
    Serial.println("Help Command List Command 1");
    Serial.println("Enter commands ##,arg;data ^Z to end  3 second time out on data");
    Serial.println("1; = list commands");
    Serial.println("2,b; = Set led 1=on 0=off");
    Serial.println("3; = blink led");
    Serial.println("4,b; turn on debug 1=on 0=off");
    Serial.println("==== SPIFFS =====");
    
    Serial.println("10; = directory of spiffs");
    Serial.println("11; = spiffs information");
    Serial.println("12, = read spiffs file 12,/path/filename,null=full speed, L line mode ;  data begins with ^B ends with ^Z  enter X to abort");
    Serial.println("13, = write data to spiffs file 13,/path/filename; data ... ^Z ");
    Serial.println("14, = delete spiffs file 13,/path/filename;");
    Serial.println("19,b; = format may take 1 min spiffs info when done");

    Serial.println("====== WiFi ======");
    Serial.println("20, = set Wifi SSID and Password 20,ssid,pw;");
    Serial.println("21; = connect to wifi");
    Serial.println("22; = disconnect from wifi");
    Serial.println("23; = wifi status information");
    Serial.println("24; = wifi scan for networks");
    
    Serial.println("====== FTP ======");
    Serial.println("30,ip,un,ps,dir; = FTP user name and password and directory");
    Serial.println("31; = FTP connect");
    Serial.println("32; = FTP disconnect");
    Serial.println("33, = FTP upload  33,/spiffs_filename.ext,/ftp_filename.ext;");
    Serial.println("34, = FTP download 34,/spiffs_filename.ext,/ftp_filename.ext;");
    Serial.println("35; = FTP Directory");
    Serial.println("36; = FTP Delete 36,/ftp_filename.ext;");
    Serial.println("================");

    Serial.println("====== CHIP API ======");
    Serial.println("40; = Deep sleep command");
    Serial.println("OK,1");
    

}
//=========
void cmd2_toggleled()
{ ledState = cmdMessenger.readBoolArg();    // Read led state argument, interpret string as boolean
  Serial.print("Cmd 1 Set led to ");
  Serial.println(ledState);
  ledState = !ledState;                     // Set led
  digitalWrite(RED_LED_PIN, ledState ? HIGH : LOW);
  blink_flag = 0;
}
//========
void cmd3_blinkled()
{
  Serial.println("Cmd 3 blink");
  blink_flag = 1;
  blink_cntr = 1, 000;
}
//========
void cmd4_debug()
{
    debug_flag = cmdMessenger.readBoolArg();
    Serial.print("OK,4,");
    Serial.println(debug_flag);
}
//=========
void cmd10_dir_spiffs_file()
{
  Serial.println("==== Directory ====");  
  Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {
    Serial.print(dir.fileName());
    Serial.print("   Size = ");
    File f = dir.openFile("r");
    Serial.println(f.size());
    }
  f.close();
  Serial.println("=============");
  Serial.println("OK,10");
}
//=========
void  cmd11_spiffs_info() 
{
    spiffs_info();
    Serial.println("OK,11");
}
//=========
void cmd12_read_spiffs_file()               //COMMAND 12
{
  String  read_mode;
  byte  char_read_mode;
   
  file_name = cmdMessenger.readStringArg();                   // Note that readStringArg behaves a bit differently,in that the string is only available as long as no new command has been read.
  read_mode = cmdMessenger.readStringArg();  

  f = SPIFFS.open(file_name, "r");
  if (!f)
  {   if(debug_flag) 
      {     Serial.print("Failed to open ");
            Serial.println(file_name);
      }
      Serial.println("ER,12,SFO."); 
  }else
  {   if(debug_flag)                                  // file opened and reading
      {   Serial.print("Reading file ");
          Serial.println(file_name);  
      }
      char_read_mode=read_mode[0];
      
      switch (char_read_mode)
      {   case 'L':
          {   spiffs_line_mode();
              break;
          }
          case 'P':
          {     spiffs_page_read();
                break;  
          }
          default :
          {     spiffs_read_continous();
                break;        
          }
      }
  }
}
  //*****************************
  void  spiffs_line_mode()
  {
    bool  abort_flag;
    bool  time_out_flag;
    bool  next_file_flag;
    byte  file_num;
    char  in_char;
    char  new_file_name[20];
    char  ch;
    byte  timeout_cntr;
    
          if(debug_flag)  Serial.print("Mode = Line " );        // file opened and reading                                  
 
          next_file_flag=true;
          file_num=0;
          time_out_flag=false;
          abort_flag=false;
          Serial.println("^B");             //print beginning of file
          while(  (next_file_flag==true) && (abort_flag==false) && (time_out_flag==false)  )      // while loop for multiple files
          {       
              while (f.available() && (abort_flag==false) && (time_out_flag==false)  )          //while loop for end of file
              {   
                  ch=f.read();                      // read character from file
                  Serial.print(ch);
                  if ( Serial.available())
                  {   in_char=Serial.read();
                      if (in_char=='X') abort_flag=true;
                  }
                  if( ch == '\n' )
                  {     in_char=' ';
                        ticker_reached=false;
                        timeout_cntr=0;                             
                        while (  (time_out_flag == false) && (abort_flag==false) && !(in_char=='L')  )      //while loop wait for L for next line
                        {  
                            if ( Serial.available()) in_char=Serial.read();
                            if (in_char=='X') abort_flag=true;
                            if(ticker_reached==true)                                 //time out counter .5 seconds * 12 = 6 seconds
                            {       ticker_reached=false;
                                    if( ++timeout_cntr > 10)
                                    {     time_out_flag=true;
                                    }  
                            }
                        } 
                  } 
                }
                if ( (abort_flag==false) && (time_out_flag==false))
                {
                      f.close();
                      ++file_num;                                                   //Next file             
                      sprintf(new_file_name,"%s%02d",file_name,file_num);
                      f = SPIFFS.open(new_file_name, "r");
                      if (!f)
                      {     next_file_flag=false;
                      }else
                      {   if(debug_flag)                                  // file opened and reading
                          {   Serial.print("Reading file ");
                          Serial.println(new_file_name); 
                          }
                      }
                }               
            }

            f.close();
            if(time_out_flag==true)
            {     Serial.println("ER,12,TMOT");
                  return;
            }
            if(abort_flag==true)
            {     Serial.println("ER,12,ABRT");
                  return;
            }
            Serial.println("^Z");  //print end of file
            Serial.println("OK,12");
  }
  //*****************************
  void  spiffs_page_read()
  {
    bool  abort_flag = false;
    bool  time_out_flag = false;
    bool  next_file_flag;
    byte  file_num;
    char  in_char;
    char  new_file_name[20];
    char  ch;
    int  char_cnt;
    byte  timeout_cntr=0;
             
          if(debug_flag) Serial.print("Mode = Page" );        // file opened and reading                                 
          next_file_flag=true;
          file_num=0;
          char_cnt=0;
          abort_flag=false;
          Serial.println("^B");  //print beginning of file
          while(  (next_file_flag==true) && (abort_flag==false) && (time_out_flag==false)  )      // while loop for multiple files
          {     while (f.available() && (abort_flag==false) && (time_out_flag==false)  )          //while loop for end of file
                {   
                      ch=f.read();                      // read character from file
                      Serial.print(ch);
                      if ( Serial.available())
                      {   in_char=Serial.read();
                          if (in_char=='X') abort_flag=true;
                      }
                      if( ++char_cnt>255)
                      {     in_char=' ';
                            ticker_reached=false;
                            timeout_cntr=0;
                            while (  time_out_flag==false  && abort_flag==false && !(in_char=='P')  )
                            {
                                if ( Serial.available())in_char=Serial.read();
                                if (in_char=='X') abort_flag=true;
                                if(ticker_reached==true)                                 //time out counter .5 seconds * 12 = 6 seconds
                                {       ticker_reached=false;
                                        if( ++timeout_cntr > 10)
                                        {     time_out_flag=true;
                                        }  
                                }
                            }
                            char_cnt=0;     
                      } 
                  }
                  if ( (abort_flag==false) && (time_out_flag==false))
                  {
                          f.close();
                          ++file_num;                                                   //Next file             
                          sprintf(new_file_name,"%s%02d",file_name,file_num);
                          f = SPIFFS.open(new_file_name, "r");
                          if (!f)
                          {     next_file_flag=false;
                          }else
                          {   if(debug_flag)                                  // file opened and reading
                              {   Serial.print("Reading file ");
                                  Serial.println(new_file_name); 
                                  Serial.println(f); 
                              }
                          }               
                    }
          }
            if(time_out_flag==true)
            {     Serial.println("ER,12,TMOT");
                  return;
            }
            if(abort_flag==true)
            {     Serial.println("ER,12,ABRT");
                  return;
            }
            Serial.println("^Z");  //print end of file
            Serial.println("OK,12");
  }
  //*****************************
  void    spiffs_read_continous()
  {
    bool  abort_flag;
    bool  next_file_flag;
    byte  file_num;
    char  in_char;
    char  new_file_name[20];
    char  ch;

          abort_flag=false;
          if(debug_flag)                                   // file opened and reading
          {     Serial.print("Mode = Continous" );
          }
          Serial.println("^B");  //print beginning of file
          next_file_flag=true;
          file_num=0;
            while(  (next_file_flag==true) && (abort_flag==false)  )
            {      while (f.available() && (abort_flag==false) )
                  {   ch=f.read();                      // read character from file
                      Serial.print(ch);
                      if ( Serial.available())
                      {   in_char=Serial.read();
                          if (in_char=='X') abort_flag=true;
                      }  
                  }
                  f.close();
                  ++file_num;                                                   //Next file             
                  sprintf(new_file_name,"%s%02d",file_name,file_num);
                  f = SPIFFS.open(new_file_name, "r");
                  if (!f)
                  {     next_file_flag=false;
                  }else
                  {   if(debug_flag)                                  // file opened and reading
                      {   Serial.println();
                          Serial.print("Reading file ");
                          Serial.println(new_file_name); 
                      }
                  }               
            }
            if(abort_flag==false)
            {    Serial.println("^Z");  //print end of file
                 Serial.println("OK,12");
            }else
            {     Serial.println("ER,12,ABRT");
            }
  }
//=========
void cmd13_write_spiffs_file()                                                              //COMMAND 13
{
      bool  save_character_flag;
      bool  end_flag;
      bool  time_out_flag;
      char  timeout_cntr;
      int  buffer_cnt;
      byte  read_buffer[256];
      String  line_mode;
      bool caret_flag;
      char c;
  
      file_name = cmdMessenger.readStringArg();
      f = SPIFFS.open(file_name, "w");                                // open file for writing 
      line_mode = cmdMessenger.readStringArg();

      if (!f)
      {   if(debug_flag)
          {
                Serial.print("* File name ");
                Serial.print(file_name);  
                Serial.println(" failed to open ");
          }
          Serial.println("ER,13,SFO ");
      }else
      {
          ticker_reached = false;
          timeout_cntr=0;  
          buffer_cnt=0;
          time_out_flag=false;
          end_flag=false;
          caret_flag=false;
    
          while ( (time_out_flag==false) && (end_flag==false)  )
          {
              while ( (Serial.available()) && (buffer_cnt<256)    )
              {
                    ticker_reached = false;
                    timeout_cntr=0;
                    if(caret_flag)
                    {
                       c=Serial.peek();
                       if(c=='Z')
                       {
                          c=Serial.read();
                          end_flag=true;
                          break;
                       }
                       caret_flag=false;
                       read_buffer[buffer_cnt++] = '^';
                       continue;
                    }
                    c=Serial.read();
                    if(c=='^')
                    {
                      caret_flag=true;  
                    }
                    else
                    {
                      read_buffer[buffer_cnt] = c;
                      if(read_buffer[buffer_cnt++]=='\n')
                        break;
                    }
              }
              if(buffer_cnt!=0)
              {
                f.write(read_buffer,buffer_cnt);
              }

              if(ticker_reached==true)                                 //time out counter .5 seconds * 6 = 3 seconds
              {   ticker_reached=false;
                  if (++timeout_cntr > 12) time_out_flag=true;    
              }
             
              if  (  (end_flag != true) && (time_out_flag!=true) && (buffer_cnt!=0) )
              {
                    if(read_buffer[buffer_cnt-1]=='\n')
                    {
                      if(line_mode.charAt(0)=='L')Serial.print("L");         //print L if in line mode and received a \n
                    }
              }          
              buffer_cnt=0;
          }
          if (time_out_flag == true )
          {     if(debug_flag) Serial.println(" closed ... timed out ");
                Serial.println("ER,13,TMOT");
       
          }else
           {
                if(debug_flag) Serial.println(" file written OK");
                Serial.println("OK,13");
                Serial.println();
           }
           f.close();
          }
} 
//========
void cmd14_delete_file()
{
  file_name = cmdMessenger.readStringArg();
  Serial.println();
  if (SPIFFS.remove(file_name)) 
  {     if(debug_flag)
        {     Serial.print("Deleted file  ");
              Serial.println(file_name);
        }
        Serial.println("OK,14");
  }else
  {     if(debug_flag)
        {     Serial.print(file_name);
              Serial.println("   File not found ");
        }
        Serial.println("ER,14,SFNF");
  }
}
//=========
void cmd19_format_spiffs()
{   bool  passcode;
  passcode = cmdMessenger.readStringArg();
  if(passcode)
  {
      Serial.println("ST,19"); 
        if(debug_flag)
        {   Serial.println ("* Beginning to format....");
            Serial.print(" ...,  will take up to 1 minuite to format");
            Serial.println();
        }
      SPIFFS.format();
       if(debug_flag)
       {
                  Serial.println("  Format complete");
       }
      Serial.println("OK,19");
      if(debug_flag) { spiffs_info(); }     
  }else
  {   Serial.println();
      Serial.println("* Bad format passcode");
  }
}
//=========
void  spiffs_info()
{   FSInfo fs_info;
    SPIFFS.info(fs_info);
      Serial.println("==== File System Info =====");
      Serial.print("totalBytes = ");
      Serial.println(fs_info.totalBytes);
      Serial.print("usedBytes = ");
      Serial.println(fs_info.usedBytes);
      Serial.print("blockSize = ");
      Serial.println(fs_info.blockSize);
      Serial.print("pageSize = ");
      Serial.println(fs_info.pageSize);
      Serial.print("maxOpenFiles = ");
      Serial.println(fs_info.maxOpenFiles);
      Serial.print("totalBytes = ");
      Serial.println(fs_info.totalBytes);
      Serial.print("maxPathLength = ");
      Serial.println(fs_info.maxPathLength);
      Serial.println("==== end info =====");
}
//=========
void  cmd20_wifi_unpw()
{ 
    wifi_ssid = cmdMessenger.readStringArg();
    wifi_password = cmdMessenger.readStringArg();

    f = SPIFFS.open("/wifi.ini", "w");                                // open file for writing 
    f.print(wifi_ssid);
    f.print(",");
    f.print(wifi_password);
    f.close(); 
    Serial.println("OK,20");
    if(debug_flag)
    {
        f = SPIFFS.open("/wifi.ini", "r");            //open the file and read the ssid and password
        wifi_ssid=f.readStringUntil(',');          // read strings from file
        if (debug_flag)Serial.println(wifi_ssid);
        wifi_password=f.readStringUntil(',');          // read strings from file
        if (debug_flag) Serial.println(wifi_password);  
    }      
}
//=========
void    cmd21_wifi_connect()
{
    int    timeout_cntr;
    f = SPIFFS.open("/wifi.ini", "r");            //open the file and read the ssid and password
    if (!f)
    {   Serial.print("Failed to open ");
        Serial.println(file_name);  
    }else
    {   Serial.println("ST,21");
        while (f.available())
        {   wifi_ssid=f.readStringUntil(',');          // read strings from file
            if (debug_flag)Serial.println(wifi_ssid);
            wifi_password=f.readStringUntil(',');          // read strings from file
            if (debug_flag) Serial.println(wifi_password);        
        }
        if(debug_flag)Serial.println();
        if(debug_flag)Serial.print("Connecting to ");
        if(debug_flag)Serial.println(wifi_ssid);
      
        WiFi.begin(wifi_ssid.c_str(), wifi_password.c_str());
                // WiFi.begin("ICC", "Iccwifi1234");
       
       ticker_reached = false;
       timeout_cntr=0;
       while ( (WiFi.status() != WL_CONNECTED ) && (timeout_cntr < 20)   )
        {
            delay(500);
            if (ticker_reached)
            {   ++timeout_cntr;
                ticker_reached=0;
                if(debug_flag)Serial.print(".");
            }    
        }
        if  (WiFi.status() == WL_CONNECTED )
        {   if(debug_flag) wifi_status_print();
            Serial.println("OK,21");
        
        }else
        {   if(debug_flag)Serial.println("WiFi failed to connect");
            Serial.println("ER,21,WFTC");
        }     
    }
}
//=========
void  wifi_status_print()
{
    if( WiFi.status() != WL_CONNECTED )
    {   Serial.println("");
        Serial.println("WiFi Not Connected");  
    }else
    {   Serial.println("");
        Serial.print("WiFi connected to ");
        Serial.print(wifi_ssid);
        Serial.print(" ... IP address: ");
        Serial.println(WiFi.localIP());
    }
}
//=========
void  cmd22_wifi_disconnect()
{
      WiFi.disconnect();
      if(debug_flag) wifi_status_print() ; 
      Serial.println("OK,22");
}
//=========
void  cmd23_wifi_status()
{
        if (debug_flag)wifi_status_print();
        Serial.print("OK,23,");
        if( WiFi.status() != WL_CONNECTED )
        {   Serial.println("CON ");
        }else
        {   Serial.println("DCN ");
        }
        
}
//=========
void  cmd24_wifi_scan()
{
      Serial.println("** Scan Networks **");
      byte numSsid = WiFi.scanNetworks();
      Serial.print("SSID List:");
      Serial.println(numSsid);      
    for (int thisNet = 0; thisNet < numSsid; thisNet++)       // print the network number and name for each network found:
    { Serial.print(thisNet);
      Serial.print(") ");
      Serial.print(WiFi.SSID(thisNet));
      Serial.print("\tSignal: ");
      Serial.print(WiFi.RSSI(thisNet));
      Serial.print(" dBm");
      Serial.print("\tEncryption: ");
      printEncryptionType(WiFi.encryptionType(thisNet));
    }
    Serial.println("OK,24");
    
}
    //=========
     void printEncryptionType(int thisType)
    {
      // read the encryption type and print out the name:
      switch (thisType)
      {
        case ENC_TYPE_WEP:
          Serial.println("WEP");
          break;
        case ENC_TYPE_TKIP:
          Serial.println("WPA");
          break;
        case ENC_TYPE_CCMP:
          Serial.println("WPA2");
          break;
        case ENC_TYPE_NONE:
          Serial.println("None");
          break;
        case ENC_TYPE_AUTO:
          Serial.println("Auto");
          break;
      }
    }
//=========
void  cmd30_ftp_ip_un_pw()                                        //command 30
{ 
    ftp_ip = cmdMessenger.readStringArg();
    ftp_un = cmdMessenger.readStringArg();
    ftp_pw = cmdMessenger.readStringArg();
    ftp_dir = cmdMessenger.readStringArg();

    f = SPIFFS.open("/ftp.ini", "w");                                // open file for writing 
    f.print(ftp_ip);
    f.print(",");
    f.print(ftp_un);
    f.print(",");
    f.print(ftp_pw);
    f.print(",");
    f.print(ftp_dir);
    f.close(); 
    if (debug_flag)
    {
        Serial.println();
        Serial.println("OK ftp.ini written");
    }else
    {   Serial.println("OK,30");
    }
}
//=========
void  cmd31_ftp_connect()
{   int   command_seq;
    bool  ftp_cmd_error;
    bool  ftp_cmd_seq_end;
    String  command_str;
    
    Serial.println("ST,31");                        //start  
    if  (ftp_open_flag==true)
    {     Serial.println("OK,31,OP..");
          return;   //exit routine  
    } 
      
    f = SPIFFS.open("/ftp.ini", "r");               //open the ftp ini file
    if (!f)
    {   Serial.print("Failed to open ");
        Serial.println(file_name);  
    }else
    {   while (f.available())
        {   ftp_ip=f.readStringUntil(',');          // read ip address file
            ftp_un=f.readStringUntil(',');          // read username from file
            ftp_pw=f.readStringUntil(',');          // read password from file
            ftp_dir=f.readStringUntil(',');         // read ftp directory from file
            if (debug_flag)
            {     Serial.println("======= From /FTP.ini =========");
                  Serial.print("Server ip = ");
                  Serial.println(ftp_ip);
                  Serial.print("FTP user name = ");
                  Serial.println(ftp_un);
                  Serial.print("FTP password = "); 
                  Serial.println(ftp_pw);
                  Serial.print("FTP directory = "); 
                  Serial.println(ftp_dir);
                  Serial.println("======= end /FTP.ini =========");                  
            }          
        }
        f.close();    //close ftp ini file
        
        command_seq=1;
        ftp_cmd_seq_end=false;
        ftp_cmd_error=false;
        
        while ( (ftp_cmd_error==false)  && (ftp_cmd_seq_end==false) )
        {    
               switch (command_seq)
              {
                  case 1:
                      {   if ( ftp_cmd_client.connect(ftp_ip.c_str(), 21) )   // 21 = FTP server
                          {
                              if (debug_flag==1) Serial.println("Command connected");
                          }else
                          {
                             if (debug_flag==1)Serial.println("Command connection failed");
                             ftp_cmd_error=true;
                          }
                          if (!eRcv()) ftp_cmd_error==true;
                          ++command_seq;
                          break;
                    }
                  case 2:
                      {   command_str="User ";
                          command_str=command_str+ftp_un;
                          if (debug_flag) Serial.println(command_str);
                          ftp_cmd_client.println(command_str);
                          if (!eRcv()) ftp_cmd_error==true; 
                          ++command_seq;
                          break;
                      }
                 case 3:
                      {
                        command_str="PASS ";
                        command_str=command_str+ftp_pw;
                        if (debug_flag) Serial.println(command_str);
                        ftp_cmd_client.println(command_str);
                        if (!eRcv()) ftp_cmd_error==true; 
                        ++command_seq;
                      }
                 case 4:
                      {
                        command_str=("SYST");
                        if (debug_flag) Serial.println(command_str);
                        ftp_cmd_client.println(command_str);
                        if (!eRcv()) ftp_cmd_error==true; 
                        ++command_seq;
                      }

                  case 5:
                      {
                        command_str=("TYPE I");
                        if (debug_flag) Serial.println(command_str);
                        ftp_cmd_client.println(command_str);
                        if (!eRcv()) ftp_cmd_error==true; 
                        ++command_seq; 
                      }
                   case 6:
                      {
                        command_str=("CWD ");
                         command_str=command_str+ftp_dir;
                        if (debug_flag) Serial.println(command_str);
                        ftp_cmd_client.println(command_str);
                        if (!eRcv()) ftp_cmd_error==true; 
                        ftp_cmd_seq_end=true;
                        ++command_seq; 
                      }
                  default:
                      {
                          break;
                      }         
              } // end switch
        }//end while
        if (ftp_cmd_error)
        {   
            Serial.print("ER,31,FFTC");
            Serial.println(--command_seq);
            ftp_open_flag=false;           
        }else
        {
            Serial.println("OK,31");
            ftp_open_flag=true; 
        }
    }
}  // end of ftp connect
  
//========= eRcv =============
byte eRcv() {
  
          byte respCode;
          byte thisByte;
          byte  time_out_counter=0;

          if(debug_flag) Serial.print("In eRcv ");
          
          respCode=0;
          while (!ftp_cmd_client.available())
          {     delay(1);
                if  (ticker_reached==true)      //if not availabe after 10 seconds time out
                {     ticker_reached=false;
                      if(++time_out_counter>20)
                      {      Serial.println("ERR FTP");
                            respCode=99;
                            break;
                      }
                }
          }
        
          respCode = ftp_cmd_client.peek();
           if(debug_flag) Serial.println(respCode);
          outCount = 0;
        
          while (ftp_cmd_client.available())
          {
            thisByte = ftp_cmd_client.read();
            if(debug_flag) Serial.write(thisByte);
            if (outCount < 127)
            {
              outBuf[outCount] = thisByte;
              outCount++;
              outBuf[outCount] = 0;
            }
            else
            {
              break;     //count if too big break out
            }
          }
        
          if (respCode >= '4') {
            
            efail();
            return 0;
          }
          return 1;
        }  // eRcv()
        //=============- FTP fail =============
        void efail() {
          byte thisByte = 0;
        
          ftp_cmd_client.println(F("QUIT"));
          if (debug_flag)Serial.println("Closing FTP Connection");
          ftp_open_flag = false;

          ftp_cmd_client.stop();
          f.close();
          if(debug_flag)
          {     Serial.println("FTP fail CMD port disconnected");
                Serial.println("FTP fail SPIFS file closed");
          }else
          {     Serial.println("ER,??");
          }
        }  // efail
//=========
void  cmd32_ftp_close()
{     
    if  (ftp_open_flag==false)
    {     Serial.println("OK,32");
          return;   //exit routine  
    } 
    if (debug_flag)Serial.println("Closing FTP Connection");
    ftp_cmd_client.println("QUIT");
    if (eRcv())
    {   
        ftp_cmd_client.stop();
        if (debug_flag)Serial.println("Command disconnected");
        Serial.println("OK,32");
        ftp_open_flag=false;
        
    }else
    {   
        if (debug_flag)Serial.println("Quit Failed");
        Serial.println("ER,32,FQF ");
    }        
}
//=========
void  cmd33_ftp_upload()
{
      #define BUF_SIZE_FTP 1460                               // for faster upload increase buffer size to 1460 because 1460 is a very common MTU (well MSS), because it it corresponds to 1500, Ethernet v2's maximum, minus 20+20= 40 bytes for the IP header overhead)
      uint8_t ftp_output_buffer[BUF_SIZE_FTP];
      size_t buffer_index;
      String  command_str;
    
      if  (ftp_open_flag==false)                          //ftp closed return error
      {     Serial.println("ER,33,FCLO");
          return;   //exit routine  
      } 
       
     file_name = cmdMessenger.readStringArg();                   // Note that readStringArg behaves a bit differently,in that the string is only available as long as no new command has been read.
     ftp_file_name = cmdMessenger.readStringArg(); 
    
     f = SPIFFS.open(file_name, "r");                    //open spiffs file to read
     if (!f)
     {   if (debug_flag)
          {   Serial.print("Failed to open ");
              Serial.println(file_name);
          }
          Serial.print("ER,33,FFTO"); 
     }else
     {   if(debug_flag)                                  // file opened and reading
          {   
              Serial.print("* Sending file ");
              Serial.print(file_name);  
              Serial.print("  to ftp site"); 
              Serial.println();
          }
          
          //***** Open data port
          if ( ! open_ftp_data_port() )
          {     
                Serial.print("ER,33,FDPR");   //open data port failed
                return ;            
          }
          
          //********Send store command
          command_str="STOR ";
          command_str=command_str + ftp_file_name;
          if (debug_flag) Serial.println(command_str);
          ftp_cmd_client.println(command_str);
          if (!eRcv())
          {     
                Serial.print("ER,33,FSTO");   //store command failed    
                return ;
          }

          //********Sendiing data
          if (debug_flag) Serial.println("Sending data ...");
          buffer_index = 0;
          while (f.available())
          {
              ftp_output_buffer[buffer_index] = f.read();
              if (debug_flag) Serial.write(ftp_output_buffer[buffer_index]);
              
              buffer_index++;
              if (buffer_index > (BUF_SIZE_FTP - 1))                                                     //check for full block 
              {
                  ftp_data_client.write((const uint8_t *) &ftp_output_buffer[0], BUF_SIZE_FTP);          //send full block
                  buffer_index = 0;
                  delay(1);
              }
           }
           if (buffer_index > 0) ftp_data_client.write((const uint8_t *) &ftp_output_buffer[0], buffer_index);     //send last partial block

           //****** Ending ****
           
           f.close();
           if (debug_flag) Serial.println("SPIFS closed");
           
           ftp_data_client.stop();
           if (debug_flag) Serial.println("Data disconnected");
          
           if (!eRcv())
           {    
                Serial.println("ER,33,STOP");  
                return ;
           }
           Serial.println("OK,33");
       }
}
//=========
bool  open_ftp_data_port()
{          
          unsigned int hiPort, loPort;
          
          if (debug_flag) Serial.println("Send PASV");
          ftp_cmd_client.println("PASV");
          if (!eRcv()) return false;

          // Sample response:
          //    227 Entering Passive Mode (67,158,61,56,177,93).
          char *tStr = strtok(outBuf, "(,");            //outbut buffer contains responce from server and parses the data into 6 elements
          int array_pasv[6];
          for ( int i = 0; i < 6; i++)
          {
            tStr = strtok(NULL, "(,");
            array_pasv[i] = atoi(tStr);
            if (tStr == NULL)Serial.println("Bad PASV Answer");   //did not find the correct number of ellements
          }
                             
          hiPort = array_pasv[4] << 8;                        //moving the upper port number 93 and 177 into the port numbers
          loPort = array_pasv[5] & 255;

          if (debug_flag) Serial.print("Data port: ");
          hiPort = hiPort | loPort;
          if (debug_flag) Serial.println(hiPort);

          if (ftp_data_client.connect(ftp_ip.c_str(), hiPort))
          {
            if (debug_flag) Serial.println("Data connected");
            return  true;
          }
          else
          {
              if (debug_flag)Serial.println("Data connection failed");
              //ftp_cmd_client.stop();
              return false;
          }
          
}
//=========
void  cmd34_ftp_download()                                                                       //command 30
{     
      //#define BUF_SIZE_FTP 1460                               // for faster upload increase buffer size to 1460
      uint8_t ftp_output_buffer[BUF_SIZE_FTP];
      size_t  buffer_index;
      String  command_str;
      byte    write_buffer[256];
      int     i;
      int     file_buffer_counter;
      byte    file_num;
      char    new_file_name[20];

      
      if  (ftp_open_flag==false)                          //ftp closed return error
      {     Serial.println("ER,34,CLSD");
          return;   //exit routine  
      } 
       
     file_name = cmdMessenger.readStringArg();                   // Note that readStringArg behaves a bit differently,in that the string is only available as long as no new command has been read.
     ftp_file_name = cmdMessenger.readStringArg(); 
    
     f = SPIFFS.open(file_name, "w");                    //open spiffs file to write
     if (!f)
     {   if (debug_flag)
          {   Serial.print("Failed to open ");
              Serial.println(file_name);
          }
          Serial.print("ER,34,SOPN"); 
     }else
     {    
          check_and_delete_large_files(file_name);
          
          Serial.println("ST,34");
          if(debug_flag)                                  // file opened and reading
          {   
              Serial.print("* Getting file ");
              Serial.print(file_name);  
              Serial.print("  from ftp site"); 
              Serial.println();
          }
          //***** Open data port
          if ( ! open_ftp_data_port() )
          {     
                Serial.print("ER,34,DPRT");               //open data port failed
                return ;            
          }
          //***** send retreive command and file namee
          file_buffer_counter=0;
          ftp_cmd_client.print("RETR ");
          ftp_cmd_client.println(ftp_file_name);
          if (!eRcv())
          {     Serial.print("ER,34,FILE");              //retrive command failed    
                return ;
          }
          
          //***** get file data and save
          i=0;
          file_buffer_counter=0;
          file_num=0;
          while (ftp_data_client.connected())
          {
                while (ftp_data_client.available())
                {     
                      char c = ftp_data_client.read();
                      write_buffer[i]=c;
                      i=++i;
                      if (i==256)
                      {     f.write(write_buffer,256);
                            if (debug_flag) Serial.write(write_buffer,256);
                            i=0;
                          
                            if (++file_buffer_counter >= 160)
                            {
                                file_buffer_counter=0;
                                f.close();
                                ++file_num;               
                                sprintf(new_file_name,"%s%02d",file_name,file_num);
                                f = SPIFFS.open(new_file_name, "w");
                                if (debug_flag)
                                {     Serial.println();
                                      Serial.println(new_file_name);
                                      Serial.println();
                                }    
                            }
                      }
                }     
          }
          if(i >0 ) f.write(write_buffer,i);
          if (debug_flag) Serial.write(write_buffer,i);
           //****** Ending ****
           
           f.close();
           if (debug_flag)
           {    Serial.println();
                Serial.println("SPIFS closed");
           }
           
           ftp_data_client.stop();
           if (debug_flag) Serial.println("Data disconnected");
          
           if (!eRcv())
           {    
                Serial.println("ER,34,STOP");  
                return ;
           }
           Serial.println("OK,34");
       }
   
}
//=========
void  check_and_delete_large_files(char* file_name)
{       byte  file_num = 0;
        bool  delete_flag=true;
        char  new_file_name[20];
        
        while (delete_flag==true)
        {     
             ++file_num;
             sprintf(new_file_name,"%s%02d",file_name,file_num);     
             delete_flag=SPIFFS.remove(new_file_name);
             if(debug_flag && delete_flag==true )
             {     Serial.print("Deleted file  ");
                   Serial.println(new_file_name);
             }
       
        }
}
//=========
void  cmd35_ftp_dir()
{     
      if  (ftp_open_flag==false)
      {     Serial.println("ER,35,FTPCLS");
            return;   //exit routine  
      }
      //***** Open data port
      if ( ! open_ftp_data_port() )
      {     
            Serial.print("ER,35,DPRT");               //open data port failed
            return ;            
      }
      //***** send dir 
      
      ftp_cmd_client.println("LIST");
      if (debug_flag) Serial.print("LIST");

      if (!eRcv())
      {     Serial.print("ER,34,FILE");              //retrive command failed    
            return ;
      }
      
      //***** get file data and save
      while (ftp_data_client.connected())
      {
            while (ftp_data_client.available())
            {
                  char c = ftp_data_client.read();
                  Serial.write(c);
                  //if (debug_flag) Serial.write(c);
            }
      }
       //****** Ending ****
       ftp_data_client.stop();
       if (debug_flag) Serial.println("Data disconnected");
      
       if (!eRcv())
       {    
            Serial.println("ER,35,STOP");  
            return ;
       }
       Serial.println("OK,35");

}
//=========
void  cmd36_ftp_delete()                                                  //Command 36
{     
      if  (ftp_open_flag==false)
      {     Serial.println("ER,36,CLSD");
            return;   //exit routine  
       } 
       
       ftp_file_name = cmdMessenger.readStringArg();    //read file name
       
       //***** send delete command
       ftp_cmd_client.print("DELE ");
       ftp_cmd_client.println(ftp_file_name);
       if (!eRcv())
       {     Serial.print("ER,36,FILE");              //file name delete command failed    
              return ;
       }
       Serial.println("OK,36");
     
}
//=========
void    cmd40_deep_sleep()                                       //Command 40
{
        #define SLEEP_TIME    5
        digitalWrite(RED_LED_PIN, 1);
        digitalWrite(BLUE_LED_PIN, 1);
         if (debug_flag)
           {    Serial.println();
                Serial.println("Going to Sleep zzzz");
           }
        Serial.println("OK,40");
 /*       
  ESP.deepSleep(microseconds, mode) will put the chip into deep sleep. 
  mode is one of WAKE_RF_DEFAULT, WAKE_RFCAL, WAKE_NO_RFCAL, WAKE_RF_DISABLED. 
  deepsleepsetoption(0): Radio calibration after deep-sleep wake up depends on init data byte 108. 
  deepsleepsetoption(1): Radio calibration is done after deep-sleep wake up; this increases the current consumption.
  deepsleepsetoption(2): No radio calibration after deep-sleep wake up; this reduces the current consumption.
  deepsleepsetoption(4): Disable RF after deep-sleep wake up, just like modem sleep; 
                          this has the least current consumption; the device is not able to transmit or receive data after wake up.
  
  (GPIO16 needs to be tied to RST to wake from deepSleep.)
  see https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/pinouts
 */
      //ESP.deepSleep(SLEEP_TIME * 1000000, WAKE_RF_DEFAULT );
        ESP.deepSleep(0);  //sleeps forever          
}

