:: Written by C. Bartik
:: Written for ICC
:: Written on 7/20/2017
::
:: This script acts as a windows command-line programmer for ESP8266 based projects. 
::
:: It can be customized for specific projects by editing the values of "com" and "binName".
:: Dependent on esptool.exe and the .bin file to be programmed in the same folder as this .bat file.

@echo off

::Change these variable to the proper COM port and .bin file name/path
setlocal
set com=COM7
set binName=ICC_wifi_FTP_V27(fromEdsPC).bin

::Programmer beginning messages
echo ---------------------------------------------------------------------------
echo Welcome to the ICC Audible Reminder System programming tool.
echo ---------------------------------------------------------------------------
echo Attempting to program file %binName% using %com%

::Check if the COM port exists
mode %com% | findstr RTS > nul
if errorlevel 1 (
	echo ERROR! Could not find %com%.
	echo Make sure the COM port is correct.
	echo If its not, edit this .bat file to use the correct COM port.
	echo The COM port can be found in Device Manager while plugged in and powered.
	goto :exiterror
)

::Check if the .bin file exists within the same directory as this script
echo %com% found, locating .bin file...
dir /b /s %binName% > nul
if errorlevel 1 (
	echo ERROR! Could not find %binName%.
	echo Make sure the file name is correct, and the file is in the same directory as this script.
	echo If its not, edit this .bat file to use the correct file.
	goto :exiterror
)

::Invoke the esptool programmer
echo %binName% found, about to invoke esptool.exe for upload.
echo Put the unit into programming mode now...
pause
echo ---------------------------------------------------------------------------
echo Programming, this may take a few minutes. Please wait...
esptool.exe -vv -cd nodemcu -cb 115200 -cp %com% -ca 0x00000 -cf %binName% > log.txt
findstr /b /c:"starting app without reboot" log.txt > nul
if %errorlevel% == 0 (
	goto :exitclean
) else (
	goto :exiterror
)


::Programmer exit messages
:exiterror
echo ---------------------------------------------------------------------------
echo The programmer encountered an error and completed unsuccessfully.
goto :exitprogrammer

:exitclean
echo ---------------------------------------------------------------------------
echo The programmer completed successfully.
goto :exitprogrammer

:exitprogrammer
echo You can view the upload results in log.txt within this script's directory.
pause
