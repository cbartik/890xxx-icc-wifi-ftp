programmer.bat is written to be used simply by double clicking on the file. 

programmer.bat will need to be edited (near the top, after the comments section) to use the right
	COM port and .bin file name, allowing it to be adaptable to different PCs and FTDI programmers.
	
In addition to the esptool.exe, programmer.bat requires the latest version of the .bin file for 
	which ever project is being programmed to be manually located by the User and copied to this 
	same directory. 
	
	
Written by C. Bartik for ICC